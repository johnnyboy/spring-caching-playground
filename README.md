#### spring-caching-playground

Spring cache integration, using [EhCache](http://www.ehcache.org/) provider  
Project showcases basic caching setup and the use of various cache annotations

* @Cacheable
* @CachePut
* @CacheEvict

Featured set of integration tests demonstrates cache statistics usage for precise and robust assertions  
Simple benchmark class compares repeated caching reads vs non-caching ones
 