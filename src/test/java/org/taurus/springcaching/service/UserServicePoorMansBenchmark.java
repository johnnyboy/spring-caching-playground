package org.taurus.springcaching.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.taurus.springcaching.domain.User;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration("classpath:application-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserServicePoorMansBenchmark {

	@Autowired
	@Qualifier("cachingUserService")
	private UserService cachingUserService;

	@Autowired
	@Qualifier("defaultUserService")
	private UserService defaultUserService;

	private Collection<User> generate(UserService userService, int startingIndex) {
		return IntStream.rangeClosed(startingIndex, startingIndex + 999).mapToObj(idx -> userService.save(generateUser(idx))).collect(Collectors.toSet());
	}

	private User generateUser(int index) {
		return new User("name".concat(String.valueOf(index)), "email".concat(String.valueOf(index)).concat("@hotmail.com"));
	}

	private void defaultReads() {
		Collection<User> users = generate(defaultUserService, 1);
		repeatableRead(users, defaultUserService, 100);
	}

	private void repeatableRead(Collection<User> users, UserService userService, int times) {
		IntStream.rangeClosed(1, times).forEach(i -> users.forEach(u -> userService.findOne(u.getId())));
	}

	private void cachingReads() {
		Collection<User> users = generate(cachingUserService, 1001);
		repeatableRead(users, cachingUserService, 100);
	}

	@Test
	public void benchMarkDefaultVsCachingRead() {
		long cachingReadDuration = millis(this::cachingReads);
		long nonCachingReadDuration = millis(this::defaultReads);
		System.out.printf("%-20s %d ms%n", "Non-caching reads", nonCachingReadDuration);
		System.out.printf("%-20s %d ms%n", "Caching reads", cachingReadDuration);
		assertThat(cachingReadDuration).isLessThan(nonCachingReadDuration);
	}

	private long millis(Action action) {
		Instant start = Instant.now();
		action.execute();
		Instant end = Instant.now();
		return Duration.between(start, end).toMillis();
	}
}

@FunctionalInterface
interface Action {
	void execute();
}
