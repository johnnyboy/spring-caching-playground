package org.taurus.springcaching.service;

import org.taurus.springcaching.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration("classpath:application-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class CachingUserServiceIT {

    @Autowired
    @Qualifier("cachingUserService")
    private UserService userService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private CacheStatisticService cacheStatisticService;

    @Test
    public void clearingCacheClearsStatistics() {
        //given
        Caches userCache = Caches.USERS;

        //when
        cacheService.clear(userCache);

        //then
        assertThat(cacheStatisticService.hitCount(userCache)).isZero();
        assertThat(cacheStatisticService.missCount(userCache)).isZero();
        assertThat(cacheStatisticService.putCount(userCache)).isZero();
        assertThat(cacheStatisticService.putAddedCount(userCache)).isZero();
        assertThat(cacheStatisticService.putUpdatedCount(userCache)).isZero();
        assertThat(cacheStatisticService.removeCount(userCache)).isZero();
        assertThat(cacheStatisticService.evictedCount(userCache)).isZero();
    }

    @Test
    public void savingUserIncrementsPutCountAndPutAddedCount() {
        //given
        Caches userCache = Caches.USERS;
        long beforePutCount = cacheStatisticService.putCount(userCache);
        long beforePutAddedCount = cacheStatisticService.putAddedCount(userCache);

        //when
        userService.save(new User("Bob Bobson", "bobson@hotmail.com"));
        long afterPutCount = cacheStatisticService.putCount(userCache);
        long afterPutAddedCount = cacheStatisticService.putAddedCount(userCache);

        //then
        assertThat(afterPutCount - beforePutCount).isOne();
        assertThat(afterPutAddedCount - beforePutAddedCount).isOne();
    }

    @Test
    public void findingExistingUserIncrementsHitCount() {
        //given
        Caches userCache = Caches.USERS;
        long beforeHitCount = cacheStatisticService.hitCount(userCache);

        //when
        User user = userService.save(new User("Bob Bobson", "bobson@hotmail.com"));
        userService.findOne(user.getId());
        long afterHitCount = cacheStatisticService.hitCount(userCache);
        User cachedUser = (User) cacheService.getCache(userCache).get(user.getId()).get();

        //then
        assertThat(afterHitCount - beforeHitCount).isOne();
        assertThat(user).isEqualTo(cachedUser);
    }

    @Test
    public void findingNonExistingUserIncrementsMissCount() {
        //given
        Caches userCache = Caches.USERS;
        long nonExistingUserId = 11L;
        long beforeMissCount = cacheStatisticService.missCount(userCache);

        //when
        userService.findOne(nonExistingUserId);
        long afterMissCount = cacheStatisticService.missCount(userCache);

        //then
        assertThat(afterMissCount - beforeMissCount).isOne();
    }

    @Test
    public void updatingUserIncrementsPutUpdatedCount() {
        //given
        Caches userCache = Caches.USERS;
        long beforePutUpdatedCount = cacheStatisticService.putUpdatedCount(userCache);

        //when
        User user = userService.save(new User("Bob Bobson", "bobson@hotmail.com"));
        user.setName("Bob Robson");
        userService.save(user);
        long afterPutUpdatedCount = cacheStatisticService.putUpdatedCount(userCache);

        //then
        assertThat(afterPutUpdatedCount - beforePutUpdatedCount).isOne();
    }

    @Test
    public void deletingUserIncrementsRemoveCount() {
        //given
        Caches userCache = Caches.USERS;
        long beforeRemoveCount = cacheStatisticService.removeCount(userCache);

        //when
        User user = userService.save(new User("Bob Bobson", "bobson@hotmail.com"));
        userService.delete(user);
        long afterRemoveCount = cacheStatisticService.removeCount(userCache);

        //then
        assertThat(afterRemoveCount - beforeRemoveCount).isOne();
    }

    @Test
    public void deletingUserEvictsSingleCacheElement() {
        //given
        Caches userCache = Caches.USERS;
        User survivor = userService.save(new User("Survivor", "eye.of.the.tiger@hotmail.com"));
        User doomed = userService.save(new User("Doomed", "dust.in.the.wind@hotmail.com"));

        //when
        userService.delete(doomed);
        Object cachedDoom = cacheService.getCache(userCache).get(doomed.getId());
        User cachedSurvivor = (User) cacheService.getCache(userCache).get(survivor.getId()).get();

        //then
        assertThat(cachedDoom).isNull();
        assertThat(cachedSurvivor).isNotNull().isEqualTo(survivor);
    }

    @Test
    public void overflowingMaxCacheEntriesIncrementsEvictedCount() {
        //given
        Caches userCache = Caches.USERS;
        long beforeEvictedCount = cacheStatisticService.evictedCount(userCache);
        int maxCacheEntries = Long.valueOf(cacheService.getNativeCache(userCache).getCacheConfiguration().getMaxEntriesLocalHeap()).intValue();
        int overflow = 20;

        //when
        IntStream.range(0, maxCacheEntries + overflow).forEach(index -> userService.save(generateUser(index)));
        long afterEvictedCount = cacheStatisticService.evictedCount(userCache);

        //then
        assertThat(afterEvictedCount - beforeEvictedCount).isEqualTo(overflow);
    }

    private User generateUser(int index) {
        return new User("name".concat(String.valueOf(index)), "email".concat(String.valueOf(index)).concat("@hotmail.com"));
    }
}
