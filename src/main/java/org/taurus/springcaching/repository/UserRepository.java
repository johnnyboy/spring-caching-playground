package org.taurus.springcaching.repository;

import org.taurus.springcaching.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
