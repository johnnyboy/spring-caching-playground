package org.taurus.springcaching.service;

import org.taurus.springcaching.domain.User;
import org.taurus.springcaching.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("cachingUserService")
@Transactional
public class CachingUserService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public CachingUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @CachePut(value = "userCache", key = "#result.id")
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(value = "userCache", key = "#id")
    public User findOne(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return userRepository.count();
    }

    @Override
    @CacheEvict(value = "userCache", key = "#user.id")
    public void delete(User user) {
        userRepository.delete(user);
    }

}
