package org.taurus.springcaching.service;

public enum Caches {
    USERS("userCache");

    public final String name;

    Caches(String name) {
        this.name = name;
    }

}
