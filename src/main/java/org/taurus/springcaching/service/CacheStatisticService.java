package org.taurus.springcaching.service;

import net.sf.ehcache.statistics.StatisticsGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CacheStatisticService {

    private final CacheService cacheService;

    @Autowired
    public CacheStatisticService(CacheService cacheService) {
        this.cacheService = cacheService;

    }

    private StatisticsGateway getStatistics(Caches cacheName) {
        return cacheService.getNativeCache(cacheName).getStatistics();
    }

    public long hitCount(Caches cacheName) {
        return getStatistics(cacheName).cacheHitCount();
    }

    public long missCount(Caches cacheName) {
        return getStatistics(cacheName).cacheMissCount();
    }

    public long evictedCount(Caches cacheName) {
        return getStatistics(cacheName).cacheEvictedCount();
    }

    public long removeCount(Caches cacheName) {
        return getStatistics(cacheName).cacheRemoveCount();
    }

    public long putCount(Caches cacheName) {
        return getStatistics(cacheName).cachePutCount();
    }

    public long putAddedCount(Caches cacheName) {
        return getStatistics(cacheName).cachePutAddedCount();
    }

    public long putUpdatedCount(Caches cacheName) {
        return getStatistics(cacheName).cachePutUpdatedCount();
    }

}
