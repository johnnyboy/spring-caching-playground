package org.taurus.springcaching.service;

import net.sf.ehcache.Ehcache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    private final CacheManager cacheManager;

    @Autowired
    public CacheService(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public Cache getCache(Caches cacheName) {
        return cacheManager.getCache(cacheName.name);
    }

    public Ehcache getNativeCache(Caches cacheName) {
        return (net.sf.ehcache.Cache) getCache(cacheName).getNativeCache();
    }

    public void clear(Caches cacheName) {
        getCache(cacheName).clear();
    }
}
