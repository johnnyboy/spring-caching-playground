package org.taurus.springcaching.service;

import org.taurus.springcaching.domain.User;

public interface UserService {

    User save(User user);

    User findOne(Long id);

    long count();

    void delete(User user);
}
